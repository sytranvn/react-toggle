import styled from 'styled-components'

const transition = props => props.animated ? 'all 0.3s ease-in 0s' : 'none'

const Track = styled.span`
  display: block;
  margin-left: ${props => props.checked ? 0 : '-100%'};
  width: 200%;
  -webkit-transition: ${transition};
  -moz-transition: ${transition};
  -o-transition: ${transition};
  transition: ${transition};

  :before, :after {
    box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
    display: block;
    float: left;
    height: 15px;
    line-height: 15px;
    padding: 0;
    width: 50%;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    opacity: ${props => props.pending ? 0.5 : 1};
  }

  :before {
    background-color: #25D261;
    color: #FFFFFF;
    content: "";
    padding-left: 2px;
  }
  :after {
    background: ${props => props.pending ? '#25D261' : '#CCCCCC'};
    content: "";
    padding-right: 5px;
    text-align: right;
  }
`
export default Track
