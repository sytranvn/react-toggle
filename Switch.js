import styled from 'styled-components'

const transition = props => props.animated ? 'all 0.3s ease-in 0s' : 'none'

const Switch = styled.span`
  background-color: #fff;
  background-image: -o-linear-gradient(bottom, #DDDDDD 0%, #FFFFFF 100%);
  background-image: -moz-linear-gradient(bottom, #DDDDDD 0%, #FFFFFF 100%);
  background-image: -webkit-linear-gradient(bottom, #DDDDDD 0%, #FFFFFF 100%);
  background-image: -ms-linear-gradient(bottom, #DDDDDD 0%, #FFFFFF 100%);
  background-image: linear-gradient(to bottom, #DDDDDD 0%, #FFFFFF 100%);
  border-radius: 15px;
  bottom: 0;
  box-shadow: inset 0 1px 1px #fff;
  display: block;
  margin: 2px;
  position: absolute;
  right: ${props => props.checked ? 0 : '15px'};
  top: 0;
  width: 11px;
  -webkit-transition: ${transition};
  -moz-transition: ${transition};
  -o-transition: ${transition};
  transition: ${transition};
`
export default Switch
