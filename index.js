import React from 'react'
import PropTypes from 'prop-types'

import Wrapper from './Wrapper'
import Track from './Track'
import Switch from './Switch'

class Toggle extends React.Component {
  render () {
    const { animated, checked, disabled, onClick, pending } = this.props

    return <Wrapper disabled={disabled} onClick={!(pending || disabled) && onClick}>
      <input checked={checked} readOnly />
      <label>
        <Track animated={animated} checked={checked} pending={pending} />
        <Switch animated={animated} checked={checked} />
      </label>
    </Wrapper>
  }
}

Toggle.propTypes = {
  animated: PropTypes.bool,
  checked: PropTypes.bool,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  pending: PropTypes.bool
}

export default Toggle
