import styled from 'styled-components'

const Wrapper = styled.div`
  display: inline-block;
  position: relative;
  width: 30px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;

  input {
    display: none;
  }

  label {
    border-radius: 15px;
    cursor: ${props => props.disabled ? 'not-allowed' : 'pointer'};
    display: block;
    overflow: hidden;
    margin-bottom: 0;
  }
`

export default Wrapper
